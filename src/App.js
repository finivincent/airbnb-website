import React from "react";
import { BrowserRouter } from "react-router-dom";
import "../src/assets/css/style.css";
import Home from "./components/screens/Home";

const App = () => {
    return (
        <BrowserRouter>
            <Home />
        </BrowserRouter>
    );
};

export default App;
