import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

export default function Spotlight() {
    return (
        <Container>
            <Heading>Go Near</Heading>
            <SpotButton to="/">Explore nearby stays</SpotButton>
        </Container>
    );
}

const Container = styled.div`
    width: 20%;
    padding: 291px 0;
`;
const Heading = styled.h1`
    font-family: "Airbnb_Cereal-Bold";
    font-size: 100px;
    color: #ffff;
    line-height: 0.9;
    margin-bottom: 15px;
`;
const SpotButton = styled(Link)`
    font-size: 18px;
    padding: 8px 15px;
    background: #fff;
    border-radius: 10px;
    width: 75%;
`;
